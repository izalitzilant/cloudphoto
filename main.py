#!/home/izalit/Desktop/INTRO_INTO_CLOUDS/cloudphoto_gl/cloudphoto/.venv/bin/python
# -*- coding: utf-8 -*-
import argparse
from client import Client
from commands import cloudphoto_init_func, cloudphoto_upload_func, cloudphoto_list_func, cloudphoto_delete_func, \
    cloudphoto_download_func, cloudphoto_mksite_func
from pathlib import Path
from configparser import ConfigParser


def get_config():
    config_parser = ConfigParser()
    config_parser.read(Path.home().absolute().joinpath('.config/cloudphoto/cloudphotorc'))
    return config_parser


def add_init_subparser(cloudphoto_subparsers):
    ### init command
    init_parser = cloudphoto_subparsers.add_parser('init')
    init_parser.set_defaults(func=cloudphoto_init_func)
    ###


def add_mksite_subparser(cloudphoto_subparsers):
    ### mksite command
    list_parser = cloudphoto_subparsers.add_parser('mksite')
    list_parser.set_defaults(func=cloudphoto_mksite_func)
    ###


def add_download_subparser(cloudphoto_subparsers):
    ### download command
    list_parser = cloudphoto_subparsers.add_parser('download')
    list_parser.add_argument('--album', nargs=1,
                             metavar='ALBUM', dest='ALBUM',
                             required=True, help='upload to destination album')
    list_parser.add_argument('--path', nargs=1,
                             metavar='PHOTOS_DIR', dest='PATH',
                             required=False, help='upload photos from this path')
    list_parser.set_defaults(func=cloudphoto_download_func)
    ###


def add_delete_subparser(cloudphoto_subparsers):
    ### delete command
    list_parser = cloudphoto_subparsers.add_parser('delete')
    list_parser.add_argument('--album', nargs=1,
                             metavar='ALBUM', dest='ALBUM',
                             required=True, help='delete photos from that ALBUM')
    list_parser.add_argument('--photo', nargs=1,
                             metavar='PHOTO', dest='PHOTO',
                             required=False, help='delete photo with name PHOTO')
    list_parser.set_defaults(func=cloudphoto_delete_func)
    ###


def add_upload_subparser(cloudphoto_subparsers):
    ### upload command
    list_parser = cloudphoto_subparsers.add_parser('upload')
    list_parser.add_argument('--album', nargs=1,
                             metavar='ALBUM', dest='ALBUM',
                             required=True, help='upload to destination album')
    list_parser.add_argument('--path', nargs=1,
                             metavar='PHOTOS_DIR', dest='PATH',
                             required=False, help='upload photos from this path')
    list_parser.set_defaults(func=cloudphoto_upload_func)
    ###


def add_list_subparser(cloudphoto_subparsers):
    ### list command
    list_parser = cloudphoto_subparsers.add_parser('list')
    list_parser.add_argument('--album', nargs=1,
                             metavar='ALBUM', dest='ALBUM',
                             required=False, help='to show images of specified album')
    list_parser.set_defaults(func=cloudphoto_list_func)
    ###


def main():
    cloudphoto_parser = argparse.ArgumentParser(
        'cloudphoto',
        description='CLI for custom implementation of cloudphoto, image store management system'
                    'build with Yandex Cloud Object Storage'
    )
    cloudphoto_parser.add_argument('-v', '--version',
                                   action='version', version='%(prog)s 0.1.0')

    cloudphoto_subparsers = cloudphoto_parser.add_subparsers(
        title='commands',
        dest='parser_name')

    config_parser = get_config()

    client = Client(config_parser=config_parser)

    add_init_subparser(cloudphoto_subparsers)
    add_list_subparser(cloudphoto_subparsers)
    add_upload_subparser(cloudphoto_subparsers)
    add_delete_subparser(cloudphoto_subparsers)
    add_download_subparser(cloudphoto_subparsers)
    add_mksite_subparser(cloudphoto_subparsers)

    args = cloudphoto_parser.parse_args()

    if args.parser_name == 'list':
        args.func(client, args.ALBUM)
    elif args.parser_name == 'upload':
        args.func(client, args.ALBUM, args.PATH)
    elif args.parser_name == 'delete':
        args.func(client, args.ALBUM, args.PHOTO)
    elif args.parser_name == 'download':
        args.func(client, args.ALBUM, args.PATH)
    elif args.parser_name == 'mksite':
        args.func(client)

    exit(0)


if __name__ == '__main__':
    main()
