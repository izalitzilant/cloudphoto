import bs4


def get_index_page():
    index_page = """
                    <!doctype html>
                    <html>
                        <head>
                            <title>Photo archive</title>
                        </head>
                        <body>
                            <h1>Photo archive</h1>
                                <ul>
                                </ul>
                        </body
                    </html>
                """
    return index_page
