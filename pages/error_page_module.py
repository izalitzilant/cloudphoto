import bs4


def get_error_page():
    error_page = """
                    <!doctype html>
                    <html>
                        <head>
                            <title>Photo archive</title>
                        </head>
                        <body>
                            <h1>Error</h1>
                            <p>Error while accessing to archive. Get back to <a href="index.html">main page</a> of archive.</p>
                        </body>
                    </html>
                """
    return error_page
