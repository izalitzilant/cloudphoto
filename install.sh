#!/bin/bash

# create virtualenv
$(python -m venv .venv)

# install requirements.txt
./.venv/bin/pip install -r requirements.txt

echo -e "#!$(pwd)/.venv/bin/python\n# -*- coding: utf-8 -*-\n$(cat main.py)" > main.py


## set alias
echo -e "\n" >> ~/.bashrc
echo "# The next line creates cloudphoto alias" >> ~/.bashrc
echo "alias cloudphoto='$(pwd)/main.py'" >> ~/.bashrc


# TO REMOVE ALIAS YOU SHOULD ERASE THAT TEXT FROM YOUR ~/.bashrc FILE
