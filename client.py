import boto3
from configparser import ConfigParser


class Client:
    def __init__(self, config_parser: ConfigParser):
        self.config_parser = config_parser
        self.boto3_s3 = boto3.client(
            's3',
            aws_access_key_id=self.config_parser['DEFAULT']['aws_access_key_id'],
            aws_secret_access_key=self.config_parser['DEFAULT']['aws_secret_access_key'],
            region_name=self.config_parser['DEFAULT']['region'],
            endpoint_url=self.config_parser['DEFAULT']['endpoint_url'])
