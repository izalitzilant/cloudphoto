import json
import os
from botocore.exceptions import ClientError
from client import Client
from commands import get_album_dict, arg_to_str


def get_photos(path_to_photos_dir: str) -> list:
    photos = [i for i in os.listdir(path_to_photos_dir) if i.endswith('.jpeg') or i.endswith('.jpg')]
    return photos


def upload_images(client: Client, album_name: str, photos_list: list, path_to_photos_dir: str) -> None:
    try:
        for i in photos_list:
            client.boto3_s3.upload_file(
                Filename=path_to_photos_dir + '/' + i,
                Key=album_name + '/' + i,
                Bucket=client.config_parser['DEFAULT']['bucket']
            )
    except ClientError as e:
        print(e.response['Error']['Message'])
        exit(1)


def upload_album(client: Client, album_name: str, photos_list: list) -> None:
    try:
        client.boto3_s3.put_object(
            Key=album_name + '.json',
            Bucket=client.config_parser['DEFAULT']['bucket'],
            Body=(bytes(json.dumps({'name': album_name, 'photos': photos_list}).encode('UTF-8'))))
    except ClientError as e:
        print(e.response['Error']['Message'])
        exit(1)


def cloudphoto_upload_func(client: Client, album_arg: list, path_arg: list | None) -> None:
    album_name = arg_to_str(album_arg)
    path_to_photos_dir = arg_to_str(path_arg, './')
    photos_list = get_photos(path_to_photos_dir)
    album_dict = get_album_dict(client, album_name)
    if album_dict:
        album_dict['photos'] += photos_list
        upload_album(client, album_name, album_dict['photos'])
    else:
        upload_album(client, album_name, photos_list)
        print(f"specified album with name {album_name} and key {album_name}.json will be created")
    upload_images(client, album_name, photos_list, path_to_photos_dir)
