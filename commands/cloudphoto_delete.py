import json
from client import Client
from commands import arg_to_str, get_album_dict, get_all_objects, upload_album
from botocore.exceptions import ClientError


def delete_album_dict(client: Client, album_name: str):
    try:
        client.boto3_s3.delete_object(
            Bucket=client.config_parser['DEFAULT']['bucket'],
            Key=album_name + '.json')
    except ClientError as e:
        print(e.response['Error']['Message'])
        exit(1)


def get_objs_with_prefix(client: Client, album_name: str) -> list:
    try:
        prefixed_objs = client.boto3_s3.list_objects(
            Bucket=client.config_parser['DEFAULT']['bucket'],
            Prefix=album_name + '/'
        )['Contents']
        transformed_objs = [{'Key': i['Key']} for i in prefixed_objs]
        return transformed_objs
    except ClientError as e:
        print(e.response['Error']['Message'])
        exit(1)


def delete_album_photos(client: Client, album_name: str) -> None:
    try:
        client.boto3_s3.delete_objects(
            Bucket=client.config_parser['DEFAULT']['bucket'],
            Delete={
                'Objects': get_objs_with_prefix(client, album_name)
            }
        )
    except ClientError as e:
        print(e.response['Error']['Message'])
        exit(1)


def delete_photo(client: Client, album_name: str, photo_name: str) -> None:
    try:
        client.boto3_s3.delete_object(
            Bucket=client.config_parser['DEFAULT']['bucket'],
            Key=album_name + '/' + photo_name
        )
    except ClientError as e:
        print(e.response['Error']['Message'])
        exit(1)


def remove_photo_from_album_json(album_name, client, photo_name):
    album_dict = get_album_dict(client, album_name)
    album_dict['photos'].remove(photo_name)
    upload_album(client, album_dict['name'], album_dict['photos'])


def cloudphoto_delete_func(client: Client, album_arg: list, photo_name_arg: list | None) -> None:
    album_name = arg_to_str(album_arg)
    photo_name = arg_to_str(photo_name_arg, None)
    if photo_name:
        remove_photo_from_album_json(album_name, client, photo_name)  # EDIT ALBUM JSON OBJ
        delete_photo(client, album_name, photo_name)  # DELETE PHOTO WITH KEY ALBUM_NAME + / + PHOTO_NAME
    else:
        delete_album_photos(client, album_name)  # DELETE ALL PHOTOS WITH PREFIX ALBUM_NAME
        delete_album_dict(client, album_name)  # DELETE ALBUM JSON OBJ
