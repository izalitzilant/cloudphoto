import json
import boto3
from bs4 import BeautifulSoup
from botocore.exceptions import ClientError
from commands import get_all_objects
from pages import get_error_page, get_index_page, get_album_page
from client import Client


def upload_error_page(client: Client) -> None:
    try:
        client.boto3_s3.put_object(
            Bucket=client.config_parser['DEFAULT']['bucket'],
            Key='error.html',
            Body=get_error_page()
        )
    except ClientError as e:
        print(e.response['Error']['Message'])
        exit(1)


def get_object_by_key(client: Client, key: str):
    try:
        obj = client.boto3_s3.get_object(
            Bucket=client.config_parser['DEFAULT']['bucket'],
            Key=key)
        return obj
    except ClientError as e:
        print(e.response['Error']['Message'])
        exit(1)


def append_li_tag(client: Client, key: str, j: int, soup: BeautifulSoup):
    j += 1
    li_tag = soup.new_tag('li')
    number = str(j)
    a_tag = soup.new_tag('a', href='album' + number + '.html')
    a_tag.append('album' + number)
    li_tag.append(a_tag)
    soup.ul.append(li_tag)
    obj = get_object_by_key(client, key)
    album_page = BeautifulSoup(get_album_page(), 'html.parser')
    obj_body = json.loads(obj['Body'].read().decode())
    div_tag = album_page.div
    return soup, album_page, obj_body, div_tag, j


def add_img_tag(album_page: BeautifulSoup, client: Client, div_tag, j: int, obj_body: dict):
    for i in obj_body['photos']:
        img_tag = album_page.new_tag('img')
        img_tag.attrs = {
            'src': "https://" + client.config_parser['DEFAULT']['bucket'] + '.storage.yandexcloud.net/' + obj_body[
                'name'] + '/' + i,
            'data-title': i}
        div_tag.append(img_tag)
    key = 'album' + str(j) + '.html'
    return key


def upload_page(page: BeautifulSoup, client: Client, key: str) -> None:
    try:
        client.boto3_s3.put_object(
            Bucket=client.config_parser['DEFAULT']['bucket'],
            Body=str(page),
            Key=key)
    except ClientError as e:
        print(e.response['Error']['Message'])
        exit(1)


def upload_album_pages(client: Client):
    all_objs = get_all_objects(client)
    soup = BeautifulSoup(get_index_page(), 'html.parser')
    j = 0
    for obj in all_objs:
        if obj['Key'].endswith('.json'):
            soup, album_page, obj_body, div_tag, j = append_li_tag(client, obj['Key'], j, soup)
            key = add_img_tag(album_page, client, div_tag, j, obj_body)
            upload_page(album_page, client, key)
    upload_page(soup, client, 'index.html')


### NO NEED TO USE
# def get_read_bucket_policy(client: Client) -> dict:
#     policy_payload = {
#         'Version': '2012-10-17',
#         'Statement': [{
#             'Sid': 'Allow public access to all objects',
#             'Effect': 'Allow',
#             'Principal': '*',
#             'Action': 's3.GetObject',
#             'Resource': 'arn:aws:s3:::' + client.config_parser['DEFAULT']['bucket'] + '/*'
#         }]
#     }
#     return policy_payload


def get_s3_resource(client):
    bucket_policy = boto3.resource(
        's3',
        aws_access_key_id=client.config_parser['DEFAULT']['aws_access_key_id'],
        aws_secret_access_key=client.config_parser['DEFAULT']['aws_secret_access_key'],
        endpoint_url=client.config_parser['DEFAULT']['endpoint_url'],
        region_name=client.config_parser['DEFAULT']['region']
    )
    return bucket_policy


### NO NEED TO USE
# def upload_bucket_policy(client: Client, bucket_policy_dict: dict) -> None:
#     try:
#         s3_resource = get_s3_resource(client)
#         bucket_policy = s3_resource.BucketPolicy(client.config_parser['DEFAULT']['bucket'])
#         bucket_policy.put(Policy=json.dumps(bucket_policy_dict))
#     except ClientError as e:
#         print(e.response['Error']['Message'])
#         exit(1)
###

def get_default_website_bucket_payload_policy():
    website_payload = {
        'ErrorDocument': {
            'Key': 'error.html'
        },
        'IndexDocument': {
            'Suffix': 'index.html'
        }
    }
    return website_payload


def create_static_website(client: Client):
    try:
        s3_resource = get_s3_resource(client)
        return s3_resource.BucketWebsite(bucket_name=client.config_parser['DEFAULT']['bucket'])
    except ClientError as e:
        print(e.response['Error']['Message'])
        exit(1)


def upload_web_site_payload(website):
    try:
        website.put(WebsiteConfiguration=get_default_website_bucket_payload_policy())
    except ClientError as e:
        print(e.response['Error']['Message'])
        exit(1)


def cloudphoto_mksite_func(client: Client) -> None:
    upload_error_page(client)
    upload_album_pages(client)
    # upload_bucket_policy(client, get_read_bucket_policy(client))
    bucket_website = create_static_website(client)
    upload_web_site_payload(bucket_website)
