from botocore.exceptions import ClientError
from client import Client


def get_all_objects(client: Client) -> list:  # getting all objects from S3
    try:
        return client.boto3_s3.list_objects(Bucket=client.config_parser['DEFAULT']['bucket'])['Contents']
    except ClientError as e:
        print(e.response['Error']['Message'])
        exit(1)


def get_album_dict(client: Client, album_name: str) -> dict | None:
    try:
        album_dict = eval(client.boto3_s3.get_object(
            Key=album_name + ".json",
            Bucket=client.config_parser['DEFAULT']['bucket'])['Body'].read().decode('utf-8'))
        return album_dict
    except ClientError as e:
        print(e.response['Error']['Message'])
        return None


def arg_to_str(arg_name_list: list | None, default_value: str | None = None) -> str | None:
    if arg_name_list:
        return arg_name_list[0]
    else:
        return default_value


from commands.cloudphoto_init import *
from commands.cloudphoto_upload import *
from commands.cloudphoto_list import *
from commands.cloudphoto_delete import *
from commands.cloudphoto_mksite import *
from commands.cloudphoto_download import *
