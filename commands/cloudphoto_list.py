from botocore.exceptions import ClientError
from client import Client
from commands import get_album_dict


def print_all_albums(contents: dict) -> None:
    for key in contents:
        if str(key['Key']).endswith('.json'):  # finding a .json files(albums) in response
            print(key['Key'][:-5])  # to remove .json postfix


def get_all_objects(client: Client) -> list:  # getting all objects from S3
    try:
        response = client.boto3_s3.list_objects(Bucket=client.config_parser['DEFAULT']['bucket'])
        if 'Contents' in response:
            return response['Contents']
        else:
            return []
    except ClientError as e:
        print(e.response['Error']['Message'])
        exit(1)


def print_all_album_photos(album: dict) -> None:
    for i in album['photos']:
        print(i)


def cloudphoto_list_func(client: Client, album_name_arg: list | None = None) -> None:
    if album_name_arg:
        album_dict = get_album_dict(client, album_name_arg[0])
        if album_dict:
            print_all_album_photos(album_dict)
    else:
        contents = get_all_objects(client)
        print_all_albums(contents)
