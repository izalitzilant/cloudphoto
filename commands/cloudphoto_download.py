from botocore.exceptions import ClientError
from client import Client
from commands import arg_to_str


def get_album_objs(album_name, client):
    try:
        album_objects = client.boto3_s3.list_objects_v2(
            Bucket=client.config_parser['DEFAULT']['bucket'],
            Prefix=album_name + '/')['Contents']
        return album_objects
    except ClientError as e:
        print(e.response['Error']['Message'])
        exit(1)


def download_objs(album_name, album_objects, client, path_str):
    try:
        for object in album_objects:
            client.boto3_s3.download_file(
                Bucket=client.config_parser['DEFAULT']['bucket'],
                Key=object['Key'],
                Filename=path_str + '/' + object['Key'].removeprefix(album_name + '/'))
    except ClientError as e:
        print(e.response['Error']['Message'])
        exit(1)


def cloudphoto_download_func(client: Client, album_arg: list, path_arg: list) -> None:
    album_name = arg_to_str(album_arg)
    path_str = arg_to_str(path_arg, './')
    try:
        album_objects = get_album_objs(album_name, client)
        download_objs(album_name, album_objects, client, path_str)
    except ClientError as e:
        print(e.response['Error']['Message'])
        exit(1)
