import configparser
import os
from pathlib import Path

CONFIG_PATH = str(os.path.join(str(Path.home()), '.config', 'cloudphoto'))


def input_config_data():
    config = configparser.ConfigParser()
    config.read(CONFIG_PATH)
    config['DEFAULT']['bucket'] = input("input_bucket_name: ")
    config['DEFAULT']['aws_access_key_id'] = input("INPUT_AWS_ACCESS_KEY_ID: ")
    config['DEFAULT']['aws_secret_access_key'] = input("INPUT_AWS_SECRET_ACCESS_KEY: ")
    config['DEFAULT']['region'] = "ru-central1"
    config['DEFAULT']['endpoint_url'] = "https://storage.yandexcloud.net"
    return config


def cloudphoto_init_func():
    config_file = Path(os.path.join(CONFIG_PATH, 'cloudphotorc'))       # trying to find a config file
    if not os.path.exists(config_file):                                 # if it wasn't found
        os.makedirs(CONFIG_PATH)                                        # creating config path
        config_file.touch(exist_ok=True)                                # creating file
    cloudphoto_config = input_config_data()                             # getting config_data from stdin
    with open(config_file, 'w') as cloudphoto_config_file:              # writing to file
        cloudphoto_config.write(cloudphoto_config_file)
